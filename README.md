# EZXR Spatial Computing

EZXR Spatial Computing 易现空间计算Unity Package manager。

## 对应版本

| Unity Version | EZXR Spatial Computing |
| ------------- | ----------- |
| 2022.3        | 1.1.0       |

## Release Note

### V1.1.0

1. 提供android EZXR Spatial Computing 易现空间计算 SDK
2. 提供C#调用算法接口SpatialComputingController类

## API 说明
