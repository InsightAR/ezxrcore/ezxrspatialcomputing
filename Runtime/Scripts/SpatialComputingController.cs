using System;
using UnityEngine;
using EzxrCore.Common;
using AOT;
using System.Collections.Generic;
using UnityEngine.UI;

namespace EZXRCoreExtensions.SpatialComputing
{
    public class SpatialComputingController : MonoBehaviour
    {
        /// <summary>
        /// 定位资源的URL
        /// </summary>
        public string m_url = "https://reloc-gw.easexr.com/api/alg/cloud/aw/reloc/proxy?routeApp=parkc&map=arglass";
        public bool m_vpsRequestAuto = true;
        public CpuImageControllerBase m_cpuImageController;
        public bool m_imgUndistOnDevice = false;
        public DeviceType m_deviceType = DeviceType.EzxrGlass6dof;
        public string m_sdk_version = "1.2.0";

        /// <summary>
        /// SN Number for the device, if not update, Unity will use other ways to identifier this device.
        /// </summary>
        private string m_serial_number = null;

        public SC_VPSResultState currentVpsResultState = new SC_VPSResultState();
        [SerializeField]
        Transform m_CameraOffsetTransform;

        //for copy asset
        /* iOS is not necessary
        public string assetsDirectory_ios = "";
        */
        public string assetsDirectory_android = "";
        public List<string> filesToCopy_android = new List<string>() { "", "", ""};
        public Text m_CopyAssetInfoText;

        private IntPtr m_spatialComputingHandle;
        private bool m_vpsRequestManualOnce = false;
        private double m_timestampNow = 0;
        private Matrix4x4 m_cameraTransformNow;

        private Matrix4x4 m_trackFrameTransformNow;
        private Matrix4x4 m_trackFrameToCameraOffset = Matrix4x4.identity;
  
        private CopyStreamingAssets m_CopyStreamingAssets;
        private bool m_isCopyCompleted = false;

        void Awake()
        {
            if(m_cpuImageController != null) 
            {
                m_cpuImageController.OnDeviceInfo += OnDeviceInfoReceived;
            }
        }

        void Start()
        {
            if (m_cpuImageController != null)
            {
                m_cpuImageController.OnImageFrame += OnImageFrameReceived;
                m_cpuImageController.OnTrackFrame += OnTrackFrameReceived;
                m_cpuImageController.OnTrackToImagePose += OnTrackToImagePoseReceived;
            }

            EzxrCloudLocalization.Instance.OnResponeReceived += OnCloudLocResponseReceived;
            EzxrCloudLocalization.Instance.m_url = m_url;
            if (m_CopyStreamingAssets == null)
            {
                m_CopyStreamingAssets = gameObject.AddComponent<CopyStreamingAssets>();
                if (m_CopyStreamingAssets != null)
                {
                    /* iOS is not necessary
                    m_CopyStreamingAssets.assetsDirectory_ios = assetsDirectory_ios;
                    */
                    m_CopyStreamingAssets.assetsDirectory_android = assetsDirectory_android;
                    m_CopyStreamingAssets.filesToCopy_android = filesToCopy_android;
                    m_CopyStreamingAssets.m_CopyAssetInfoText = m_CopyAssetInfoText;
                    
                    m_CopyStreamingAssets.OnCopyCompleted += OnCopyCompleted;
                }
            }
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                m_deviceType = DeviceType.ARKit;
            }
            Loom.Initialize();
            
        }

        void OnDestroy()
        {
            if (m_cpuImageController != null)
            {
                m_cpuImageController.OnImageFrame -= OnImageFrameReceived;
                m_cpuImageController.OnTrackFrame -= OnTrackFrameReceived;
                m_cpuImageController.OnTrackToImagePose -= OnTrackToImagePoseReceived;
                m_cpuImageController.OnDeviceInfo -= OnDeviceInfoReceived;
            }
            if (m_CopyStreamingAssets != null)
            {
                m_CopyStreamingAssets.OnCopyCompleted -= OnCopyCompleted;
            }
            if (m_spatialComputingHandle != IntPtr.Zero)
                SpatialComputing.ExternApi.scSpatialComputingDestroy(m_spatialComputingHandle);
            EzxrCloudLocalization.Instance.OnResponeReceived -= OnCloudLocResponseReceived;
        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnCopyCompleted(string assetPath)
        {
            if (assetPath == null)
            {
                UnityEngine.Debug.LogError("configuration resources assetPath is error");
            }
            UnityEngine.Debug.Log("OnCopyCompleted : " + assetPath);

            IntPtr t_spatialComputingHandle = SpatialComputing.ExternApi.scSpatialComputingCreate();
            if (t_spatialComputingHandle == IntPtr.Zero)
            {
                UnityEngine.Debug.LogError("scSpatialComputingCreate failed");
                return;
            }
            SpatialComputing.ExternApi.scSetRequestFrameCallingSC(t_spatialComputingHandle, OnCloudLocRequestCallback);
            SpatialComputing.ExternApi.scSetBoolDoUndistortSC(t_spatialComputingHandle, m_imgUndistOnDevice);

            string configPath = assetPath + "/localizer_fusion_config.json";
            var result = SpatialComputing.ExternApi.scSetSmoothConfig(t_spatialComputingHandle, configPath);
            Debug.Log($"scSetSmoothConfig result: {result}");
            string vps_configPath = assetPath + "/mock_vpsc_config.json";
            SpatialComputing.ExternApi.scSetVPSCConfig(t_spatialComputingHandle, vps_configPath);
            string vps_image_state_path = assetPath + "/vps_image_state.json";
            SpatialComputing.ExternApi.scSetVPSImageStateConfig(t_spatialComputingHandle, vps_image_state_path);

            //set VPSDeviceInfoC
            VPSDeviceInfoC vpsDeviceInfoC = new VPSDeviceInfoC();
            vpsDeviceInfoC.device_brand = SystemInfoHelper.GetDeivceName();
            vpsDeviceInfoC.product_name = SystemInfoHelper.GetDeviceModel();
            vpsDeviceInfoC.SN_code = m_serial_number!=null?m_serial_number:SystemInfoHelper.GetDeviceUniqueIdentifier();
            vpsDeviceInfoC.VIO_type = m_deviceType.ToString();
            vpsDeviceInfoC.SDK_version = m_sdk_version;
            SpatialComputing.ExternApi.scSetVPSDeviceInfoSC(t_spatialComputingHandle, vpsDeviceInfoC);
            m_spatialComputingHandle = t_spatialComputingHandle;
            // printf VPSDeviceInfoC
            Debug.Log("device_brand: " + vpsDeviceInfoC.device_brand + "\n" +
                        "product_name: " + vpsDeviceInfoC.product_name + "\n" +
                        "SN_code: " + vpsDeviceInfoC.SN_code + "\n" +
                        "VIO_type: " + vpsDeviceInfoC.VIO_type + "\n" +
                        "SDK_version: " + vpsDeviceInfoC.SDK_version + "\n");
        }

        public void SetVPSRequestManual(bool vpsRequestManualOnce)
        {
            m_vpsRequestManualOnce = vpsRequestManualOnce;
        }

        //for local Image test
        public void SendVPSRequestFrame(SC_InputFrame sc_input_frame)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                SpatialComputing.ExternApi.scInputVPSRequestFrameSC(m_spatialComputingHandle, sc_input_frame);
            }
        }

        public void SendVPSTrackToImagePoseOffset(float[] trackToImagePoseOffset)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                SpatialComputing.ExternApi.scSetExtrinsicImageToHead(m_spatialComputingHandle, trackToImagePoseOffset);
            }
        }

        public void GetTimestampAndCameraMatrix(out double timestamp, out Matrix4x4 cameraToWorldMatrix)
        {
            timestamp = m_timestampNow;
            cameraToWorldMatrix = m_cameraTransformNow;
        }

        public void TrigerApplyLocResultImmediately()
        {
             if (m_spatialComputingHandle != IntPtr.Zero)
             {
                  SpatialComputing.ExternApi.scTrigerApplyLocResultImmediately(m_spatialComputingHandle);
             }
        }

        public void SetRecordStoragePathAndJpgQualitySC(string path, int quality)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                SpatialComputing.ExternApi.scSetRecordStoragePathAndJpgQualitySC(m_spatialComputingHandle, path, quality);
            }
        }

        public bool StartRecordSC(string current_scence_cid)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                return SpatialComputing.ExternApi.scStartRecordSC(m_spatialComputingHandle, current_scence_cid);
            }
            return false;
        }

        public void StopRecordSC()
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                SpatialComputing.ExternApi.scStopRecordSC(m_spatialComputingHandle);
            }
        }

        void OnTrackToImagePoseReceived(Matrix4x4 trackToImagePose)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                float[] trackToImagePoseOffset = SpatialComputing.Helper.GetTrackToImagePoseOffset(trackToImagePose);
                SendVPSTrackToImagePoseOffset(trackToImagePoseOffset);
            }
            m_trackFrameToCameraOffset = trackToImagePose;
        }

        void OnTrackFrameReceived(Matrix4x4 trackFrameTransform, TrackState trackState, double timestamp, ScreenOrientation orientation)
        {
            if (m_spatialComputingHandle != IntPtr.Zero)
            {
                Pose3dMsgC pose3dMsgC = SpatialComputing.Helper.GetPose3DMsgC(trackFrameTransform, timestamp, orientation);
                //Debug.Log(SpatialComputing.Helper.Pose3dMsgCToString(pose3dMsgC));

                FusePose3dMsgC fusePose3dMsgC = new FusePose3dMsgC();
                bool success = SpatialComputing.ExternApi.scInputTrackGetFuse(m_spatialComputingHandle, pose3dMsgC, trackState, out fusePose3dMsgC);

                // TODO:
                // 1. in & out 状态过滤；
                // 2. offsettransform 可能需要修改local transform
                if (success)
                {
                    Matrix4x4 offsetMatrix = SpatialComputing.Helper.GetOffsetMatrix(fusePose3dMsgC, orientation);
                    m_CameraOffsetTransform.position = offsetMatrix.GetColumn(3);
                    m_CameraOffsetTransform.rotation = offsetMatrix.rotation;
                }
            }

            m_cameraTransformNow = trackFrameTransform * m_trackFrameToCameraOffset;
            m_trackFrameTransformNow = trackFrameTransform;
        }
        void OnImageFrameReceived(Matrix4x4 trackFrameTransform, SC_InputImage input_image, TrackState trackState, double timestamp, ScreenOrientation orientation, float angularVelocity)
        {
            if (m_spatialComputingHandle == IntPtr.Zero) return;
            
            m_timestampNow = timestamp;

            EzxrCloudLocalization.Instance.SetWidthAndHeight(input_image.width, input_image.height);

            Pose3dMsgC pose3dMsgC = SpatialComputing.Helper.GetPose3DMsgC(trackFrameTransform, timestamp, orientation);
            //Debug.Log(SpatialComputing.Helper.Pose3dMsgCToString(pose3dMsgC));

            bool vpsRequestAuto = false;

            if(m_vpsRequestAuto == true)
            {
                SC_InputFrame sc_input_frame = SpatialComputing.Helper.GetSC_InputFrame(pose3dMsgC.pose, input_image, timestamp, angularVelocity);
                Debug.Log("SC_InputImage: " + SpatialComputing.Helper.SC_InputImageToString(sc_input_frame.input_image) + "\nSC_InputFrame: angularVelocity: " + sc_input_frame.angular_velocity);
                SendVPSRequestFrame(sc_input_frame);
                vpsRequestAuto = true;
            }
      
            if (m_vpsRequestManualOnce == true && vpsRequestAuto == false)
            {
                SC_InputFrame sc_input_frame = SpatialComputing.Helper.GetSC_InputFrame(pose3dMsgC.pose, input_image, timestamp, angularVelocity);
                Debug.Log("SC_InputImage: " + SpatialComputing.Helper.SC_InputImageToString(sc_input_frame.input_image) + "\nSC_InputFrame: angularVelocity: " + sc_input_frame.angular_velocity);
                SendVPSRequestFrame(sc_input_frame);
                m_vpsRequestManualOnce = false;
                Debug.Log("m_vpsRequestManualOnce == true");
            }
        }

        void OnCloudLocResponseReceived(SC_CloudLocResult response)
        {
            if (m_spatialComputingHandle == IntPtr.Zero) return;
            Debug.Log("OnCloudLocResponseReceived" + response.result_length.ToString());
            SC_VPSResultState vpsResultState =  SpatialComputing.ExternApi.scSetVPSResultCalledSC(m_spatialComputingHandle, response);
            currentVpsResultState = vpsResultState;
            printfVPSResultState(vpsResultState);
        }

        void OnDeviceInfoReceived(string sn) {
            if(sn != null) 
                m_serial_number = sn;
        }

        void printfVPSResultState(SC_VPSResultState vpsResultState)
        {
            // EzxrCore.Log.ECLog.AddDebugLog($"ts: {vpsResultState.t_s}", 1);
            string status = "";
            switch(vpsResultState.vps_result_status) 
            {
               case LOCSTATUS.SUCCESS:
                    status = "定位成功，返回pose等信息";
                    break;
                case LOCSTATUS.FAIL_UNKNOWN:
                    status = "定位失败，走完了定位算法流程，但是图像无法定位到给定地图中";
                    break;
                case LOCSTATUS.FAIL_MATCH:
                    status = "定位失败，具体原因1 hy: not used";
                    break;
                case LOCSTATUS.FAIL_INLIER:
                    status = "定位失败，具体原因2 hy: not used";
                    break;
                case LOCSTATUS.INVALID_DEVICEINFO:
                    status = "数据不合法，传入的protobuf.deviceInfo不符合规范";
                    break;
                case LOCSTATUS.INVALID_LOCALIZER:
                    status = "数据不合法，部署阶段的localizer未成功初始化";
                    break;
                case LOCSTATUS.INVALID_IMAGE:
                    status = "数据不合法，传入的图像或protobuf.deviceInfo中出现不被接受的图像格式（仅接收通道数为1或3,且类型为CV_8U或CV_8UC3的图像）";
                    break;
                case LOCSTATUS.INVALID_IMAGE_PROTO_MATCH:
                    status = "数据不合法，传入的图像文件长度，与protobuf.deviceInfo中记录的图像字节数不匹配";
                    break;
                case LOCSTATUS.INVALID_MESSAGE:
                    status = "传入的message不合法";
                    break;
                case LOCSTATUS.FAIL_SUMMARY_UNKNOWN:
                    status = "hy: not used";
                    break;
                case LOCSTATUS.FAIL_SUMMARY_NOMAP:
                    status = "未加载完成可用的summary map hy: not used";
                    break;
            }
            // EzxrCore.Log.ECLog.AddDebugLog($"reason: {status}", 2);
            
        }

        [MonoPInvokeCallback(typeof(SC_REQUEST_CLOUDLOC_CALLBACK))]
        public static void OnCloudLocRequestCallback(SpatialComputing.SC_CloudLocRequest request_data)
        {
            Debug.Log("OnCloudLocRequestCallback " + request_data.meta.timestamp_s.ToString() + " " + request_data.byte_length.ToString() + " " + request_data.request_info_length.ToString());
            EzxrCloudLocRequestImpl request = new EzxrCloudLocRequestImpl(request_data);
            Loom.QueueOnMainThread(o =>
            {
                EzxrCloudLocalization.Instance.RequestCloudLocDirectly(request);
            }, null);
        }
    }
}